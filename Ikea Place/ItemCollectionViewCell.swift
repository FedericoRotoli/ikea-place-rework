//
//  ItemCollectionViewCell.swift
//  Ikea Place
//
//  Created by Federico Rotoli on 12/12/2019.
//  Copyright © 2019 Federico Rotoli. All rights reserved.
//

import UIKit

class ItemCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        imageView.layer.cornerRadius = 5
    }

}
