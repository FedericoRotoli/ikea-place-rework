//
//  ListController.swift
//  Ikea Place
//
//  Created by Federico Rotoli on 12/12/2019.
//  Copyright © 2019 Federico Rotoli. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension String {
    func toImage() -> UIImage? {
        if let data = Data(base64Encoded: self, options: .ignoreUnknownCharacters){
            return UIImage(data: data)
        }
        return nil
    }
}


class ListController: UIViewController {
    
    var db: [NSManagedObject] = []
    
    @IBOutlet weak var myCollection: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var collectionViewFlowLayout: UICollectionViewFlowLayout!
    let cellIdentifier = "ItemCollectionViewCell"
    
    var mark = ""
    var color = ""
    var product = ""
    var price = ""
    var image: UIImage!
    var previousModel = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetch()
        
        setupCollectionView()
        titleLabel.text = "Your wish list (\(db.count))"
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        setupCollectionViewItemSize()
    }
    
    private func setupCollectionView(){
        myCollection.delegate = self
        myCollection.dataSource = self
        let nib = UINib(nibName: "ItemCollectionViewCell", bundle: nil)
        myCollection.register(nib, forCellWithReuseIdentifier: cellIdentifier)
    }
    
    @IBAction func backTapped(_ sender: Any) {
        dismiss(animated: true, completion: {})
    }
    
    private func setupCollectionViewItemSize() {
        if collectionViewFlowLayout == nil {
            let numberOfItemPerRow: CGFloat = 3
            let lineSpacing: CGFloat = 5
            let interItemSpacing: CGFloat = 5
            
            let width = (myCollection.frame.width - (numberOfItemPerRow - 1) * interItemSpacing) / numberOfItemPerRow
            let height = width
            
            collectionViewFlowLayout = UICollectionViewFlowLayout()
            
            collectionViewFlowLayout.itemSize = CGSize(width: width, height: height)
            collectionViewFlowLayout.sectionInset = UIEdgeInsets.zero
            collectionViewFlowLayout.scrollDirection = .vertical
            collectionViewFlowLayout.minimumLineSpacing = lineSpacing
            collectionViewFlowLayout.minimumInteritemSpacing = interItemSpacing
            
            myCollection.setCollectionViewLayout(collectionViewFlowLayout, animated: true)
        }
    }
}

extension ListController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return db.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! ItemCollectionViewCell
        let furniture = db[indexPath.row]
        cell.imageView.image = (furniture.value(forKeyPath: "image") as! String).toImage()
        cell.titleLabel.text = (furniture.value(forKeyPath: "mark") as! String)

        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let furniture = db[indexPath.row]
        color = furniture.value(forKey: "color") as! String
        previousModel = furniture.value(forKey: "model") as! String
        mark = furniture.value(forKey: "mark") as! String
        product = furniture.value(forKey: "descr") as! String
        image = (furniture.value(forKey: "image") as? String)?.toImage()
        price = furniture.value(forKey: "price") as! String
        
        performSegue(withIdentifier: "wishSegue", sender: self)

    }
    
    func fetch(){
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        //2
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Furniture")
        //3
        do {
            db = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "wishSegue" {
            if let nextViewController = segue.destination as? FurnitureController {
                nextViewController.mark = mark
                nextViewController.color = color
                nextViewController.price = price
                nextViewController.product = product
                nextViewController.image = image
                nextViewController.previousModel = previousModel
                nextViewController.wish = true
               
            }
        }
        
    }
}
