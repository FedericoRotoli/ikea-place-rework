//
//  FurnitureController.swift
//  Ikea Place
//
//  Created by Federico Rotoli on 06/12/2019.
//  Copyright © 2019 Federico Rotoli. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension UIImage {
    func toString() -> String? {
        let data: Data? = self.pngData()
        return data?.base64EncodedString(options: .endLineWithLineFeed)
    }
}

class FurnitureController: UIViewController {
    
    var db: [NSManagedObject] = []
    var element: NSManagedObject!
    
    var added = false
    var wish = false
    
    var mark = ""
    var color = ""
    var product = ""
    var price = ""
    var image: UIImage!
    var previousModel = ""
    
    @IBOutlet weak var arButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var colorLabel: UILabel!
    @IBOutlet weak var productLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var wishButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if wish {
            wishButton.isHidden = true
        } else {
            check()
        }
        
        setLabel()
    }
    
    func setLabel() {
        titleLabel.text = mark
        colorLabel.text = color
        productLabel.text = product
        priceLabel.text = price + " €"
        productImage.image = image
        
        titleLabel.sizeToFit()
        colorLabel.sizeToFit()
        productLabel.sizeToFit()
        priceLabel.sizeToFit()
    }
    
    @IBAction func wishTapped(_ sender: Any) {
        
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred()
        
        if added {
            delete()
            wishButton.setImage(UIImage(systemName: "heart"), for: .normal)
            added=false
        } else {
            wishButton.setImage(UIImage(systemName: "heart.fill"), for: .normal)
            save()
            added = true
        }
        

    }
    
    @IBAction func arTapped(_ sender: Any) {
        performSegue(withIdentifier: "arSegue", sender: self)
        let generator = UIImpactFeedbackGenerator(style: .medium)
        generator.impactOccurred()
    }
    
    @IBAction func backTapped(_ sender: Any) {
        dismiss(animated: true, completion: {})
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "arSegue" {
            finalModel = previousModel
        }
    }
    
    func save() {

        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
        return
        }
        // 1
        let managedContext = appDelegate.persistentContainer.viewContext
        // 2
        let entity = NSEntityDescription.entity(forEntityName: "Furniture", in: managedContext)!
        let furniture = NSManagedObject(entity: entity, insertInto: managedContext)
        furniture.setValue(mark, forKeyPath: "mark")
        furniture.setValue(price, forKeyPath: "price")
        furniture.setValue(product, forKeyPath: "descr")
        furniture.setValue(image.toString(), forKeyPath: "image")
        furniture.setValue(previousModel, forKeyPath: "model")
        furniture.setValue(color, forKeyPath: "color")
        // 4
        do {
        try managedContext.save()
            db.append(furniture)
          } catch let error as NSError {
        print("Could not save. \(error), \(error.userInfo)")
        }


    }
    
    func check(){
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let managedContext = appDelegate.persistentContainer.viewContext
        //2

        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Furniture")
        fetchRequest.predicate = NSPredicate(format: "mark == %@", mark)
        //3
        do {
            let fetchedResults = try managedContext.fetch(fetchRequest)
            if let _ = fetchedResults.first {
                wishButton.setImage(UIImage(systemName: "heart.fill"), for: .normal)
                added = true
                element = fetchedResults.first
            }
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
            
        }
    }
    
    func delete(){
        let managedContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        managedContext.delete(element)

        do {
            try managedContext.save()
        } catch let error as NSError {
            print("Error While Deleting Note: \(error.userInfo)")
        }
    }
}
