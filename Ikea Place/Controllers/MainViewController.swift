//
//  File.swift
//  Ikea Place
//
//  Created by Federico Rotoli on 05/12/2019.
//  Copyright © 2019 Federico Rotoli. All rights reserved.
//

import Foundation
import UIKit

class MainViewController: UIViewController {
    
    var add = false
    var mark = ""
    var color = ""
    var price = ""
    var product = ""
    var previousModel = ""
    var image: UIImage!
    
    
    var timer = Timer()
    var counter = 1
    var imageArray = [UIImage(named: "furniture1"), UIImage(named: "furniture2"), UIImage(named: "furniture3"), UIImage(named: "furniture4")]
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var chairButton: UIButton!
    @IBOutlet weak var sofaButton: UIButton!
    @IBOutlet weak var closetButton: UIButton!
    @IBOutlet weak var boxButton: UIButton!
    @IBOutlet weak var myCollection: UICollectionView!
    @IBOutlet weak var myPageControl: UIPageControl!
    @IBOutlet weak var listButton: UIButton!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        myPageControl.numberOfPages = imageArray.count
        myPageControl.currentPage = 0
        
        if add {
            listButton.isHidden = true
        }
        

        setDate()
        
        prepareButton()
        
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(self.changeImage), userInfo: nil, repeats: true)
        }
        
        myCollection.delegate = self
    }
    
    @objc func changeImage() {
        if counter < imageArray.count {
            let index = IndexPath.init(item: counter, section: 0)
            self.myCollection.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
            myPageControl.currentPage = counter
            counter += 1
        } else {
            counter = 0
            let index = IndexPath.init(item: counter, section: 0)
            self.myCollection.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
            myPageControl.currentPage = counter
            counter += 1
        }
    }
    
    func prepareButton() {
        chairButton.layer.cornerRadius = 10
        chairButton.imageView?.layer.cornerRadius = 10
        sofaButton.layer.cornerRadius = 10
        sofaButton.imageView?.layer.cornerRadius = 10
        closetButton.layer.cornerRadius = 10
        closetButton.imageView?.layer.cornerRadius = 10
        boxButton.layer.cornerRadius = 10
        boxButton.imageView?.layer.cornerRadius = 10
    }
    
    func setDate() {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let dayInWeek = dateFormatter.string(from: date)
        
        let now = Date()
        dateFormatter.dateFormat = "LLLL"
        let nameOfMonth = dateFormatter.string(from: now)
        
        let date2 = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd"
        let result = formatter.string(from: date2)
        
        dateLabel.text = "\(dayInWeek) \(result) \(nameOfMonth)"
    }
    
    @IBAction func chairTap(_ sender: Any) {
        

        mark = "TERJE"
        color = "Nero"
        price = "12,95"
        product = "Sedia di legno"
        image = UIImage(named: "chairImage.png")
        previousModel = "chair"
        
        if add {
            finalModel = previousModel
            dismiss(animated: true, completion: nil)
            NotificationCenter.default.post(name: Notification.Name.Action.AddMethod, object: nil)
        } else {
            performSegue(withIdentifier: "furnitureSegue", sender: self)
        }
    }
    
    @IBAction func boxTap(_ sender: Any) {
        mark = "BOHOLMEN"
        color = "Nero / Bianco"
        price = "19,95"
        product = "Contenitore da scrivania"
        image = UIImage(named: "boxImage.png")
        previousModel = "box"
        
        if add {
            finalModel = previousModel
            dismiss(animated: true, completion: nil)
            NotificationCenter.default.post(name: Notification.Name.Action.AddMethod, object: nil)
        } else {
            performSegue(withIdentifier: "furnitureSegue", sender: self)
        }
    }
    
    @IBAction func sofaTap(_ sender: Any) {
        mark = "FRIHETEN"
        color = "Panna"
        price = "350,95"
        product = "Divano in vera pelle"
        image = UIImage(named: "sofaImage.png")
        previousModel = "sofa"
        
        if add {
            finalModel = previousModel
            dismiss(animated: true, completion: nil)
            NotificationCenter.default.post(name: Notification.Name.Action.AddMethod, object: nil)
        } else {
            performSegue(withIdentifier: "furnitureSegue", sender: self)
        }
    }
    @IBAction func closetTap(_ sender: Any) {
        mark = "LANEBERG"
        color = "Betulla"
        price = "159,95"
        product = "Armadio di legno di betulla"
        image = UIImage(named: "closetImage.png")
        previousModel = "closet"
        
        if add {
            finalModel = previousModel
            dismiss(animated: true, completion: nil)
            NotificationCenter.default.post(name: Notification.Name.Action.AddMethod, object: nil)
        } else {
            performSegue(withIdentifier: "furnitureSegue", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "furnitureSegue" {
            if let nextViewController = segue.destination as? FurnitureController {
                nextViewController.mark = mark
                nextViewController.color = color
                nextViewController.price = price
                nextViewController.product = product
                nextViewController.image = image
                nextViewController.previousModel = previousModel
            }
        }
        
    }
    
}

extension MainViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        if let vc = cell.viewWithTag(111) as? UIImageView {
            vc.image = imageArray[indexPath.row]
            vc.contentMode = .center
            vc.contentMode = .scaleAspectFill
        } else if let ab = cell.viewWithTag(222) as? UIPageControl{
            ab.currentPage = indexPath.row
        }
        
        return cell
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let offSet = scrollView.contentOffset.x
        let width = scrollView.frame.width
        let horizontalCenter = width / 2

        myPageControl.currentPage = Int(offSet + horizontalCenter) / Int(width)
        counter = myPageControl.currentPage+1
    }
}


extension MainViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame.size
        return CGSize(width: size.width, height: size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}
