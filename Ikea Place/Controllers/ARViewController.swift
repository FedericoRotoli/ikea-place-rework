//
//  ViewController.swift
//  Ikea Place
//
//  Created by Federico Rotoli on 05/12/2019.
//  Copyright © 2019 Federico Rotoli. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

var finalModel = ""

extension Notification.Name {
    struct Action {
        //notification name
        static let AddMethod = Notification.Name("addMethod")
    }
}


class ARViewController: UIViewController {
    
    let ambientLight = SCNNode()
    let myNode = SCNNode()
  
  // MARK: - Properties
    var currentAngleY: Float = 0.0
    var firstTime = true
    var firstTime2 = true
    var nodeActive = false
    var selectedNode: SCNNode!
    var focusNode: SCNNode!
    var focusPoint:CGPoint!
  
  // MARK: - Outlets
  
    @IBOutlet var sceneView: ARSCNView!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var binButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    
  // MARK: - View Management
  
  override func viewDidLoad() {
    super.viewDidLoad()
    self.initSceneView()
    self.initCoachingOverlayView()
    self.initScene()
    self.initARSession()
    self.loadModels()
    self.setButtons()
    
    NotificationCenter.default.addObserver(self, selector: #selector(addProduct), name: Notification.Name.Action.AddMethod, object: nil)
    
    }
    
    @objc func addProduct() {
        binButton.isHidden = true
        okButton.isHidden = false
        firstTime = true
        
    }
    @IBAction func takePhoto(_ sender: Any) {
        focusNode.isHidden = true
        let image = sceneView.snapshot()
        focusNode.isHidden = false
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        
        let generator = UIImpactFeedbackGenerator(style: .light)
        generator.impactOccurred()
        

        
        let alert = UIAlertController(title: "Good Job", message: "Your furniture has been successfully saved in photos!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Yes, i got it!", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
    
    func setButtons() {
        okButton.layer.cornerRadius = 0.5 * okButton.bounds.size.width
        okButton.clipsToBounds = true
    }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
  }
  
  override var prefersStatusBarHidden: Bool {
    return true
  }
    
    
    @objc func orientationChanged() {
        focusPoint = CGPoint(x: view.center.x, y: view.center.y + view.center.y * 0.25)
    }
    
  
  
  // MARK: - Initialization
  
  func initSceneView() {
    sceneView.delegate = self

    sceneView.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(rotateObject(_:))))
    
    focusPoint = CGPoint(x: view.center.x, y: view.center.y + view.center.y * 0.25)
    NotificationCenter.default.addObserver(self, selector: #selector(ARViewController.orientationChanged), name: UIDevice.orientationDidChangeNotification, object: nil)
    
  }
    @IBAction func okButtonTapped(_ sender: Any) {
        
        var animation = SCNAction()
        
        if firstTime {
            if !focusNode.isHidden {
                let modelScene = SCNScene(named: "art.scnassets/Scenes/\(finalModel).scn")!
                guard let modelNode = modelScene.rootNode.childNode(withName: finalModel, recursively: true) else {return}
                modelNode.position = focusNode.position
                modelNode.scale = SCNVector3(0,0,0)

              if finalModel == "chair" {
                  animation = SCNAction.scale(to: 0.011, duration: 0.3)
              }
              if finalModel == "sofa" {
                  animation = SCNAction.scale(to: 0.0008, duration: 0.5)
              }
             if finalModel == "closet" {
                animation = SCNAction.scale(to: 0.01, duration: 0.5)
             }
             if finalModel == "box" {
                animation = SCNAction.scale(to: 0.009, duration: 0.5)
             }

                self.sceneView.scene.rootNode.addChildNode(modelNode)


              modelNode.runAction(animation)
                modelNode.addChildNode(self.myNode)
              
              firstTime = false
                okButton.isHidden = true
              }
        } else {
            nodeActive = false
            
            okButton.isHidden = true
            binButton.isHidden = true
            
            selectedNode = SCNNode()
        }

    }
    
    @IBAction func addTapped(_ sender: Any) {
        performSegue(withIdentifier: "addSegue", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "addSegue" {
            if let nextViewController = segue.destination as? MainViewController {
                nextViewController.add = true
               
            }
        }
        
    }
    
    @IBAction func binTapped(_ sender: Any) {
        selectedNode.removeFromParentNode()
        nodeActive = false
        
        binButton.isHidden = true
        firstTime = true
    }
    
  override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
      let touch = touches.first!
      if let hit = sceneView.hitTest(touch.location(in: sceneView), options: nil).first {
        if hit.node.name != "cursor" && hit.node.name != nil {
            selectedNode = hit.node
            if !nodeActive {
                nodeActive = true

                
                okButton.isHidden = false
                binButton.isHidden = false
            }
        }
      }
  }
    
    @objc func rotateObject(_ gesture: UIPanGestureRecognizer) {

        guard let nodeToRotate = selectedNode else { return }
        if nodeToRotate.name != "cursor" && nodeToRotate.name != nil  {
            let translation = gesture.translation(in: gesture.view!)
            var newAngleY = (Float)(translation.x)*(Float)(Double.pi)/180.0
            newAngleY += currentAngleY

            nodeToRotate.eulerAngles.y = newAngleY

            if(gesture.state == .ended) {
                currentAngleY = newAngleY
            }
        }

    }
    
    @IBAction func backTapped(_ sender: Any) {
        dismiss(animated: true, completion: {})
    }
  
  func initScene() {
    let scene = SCNScene()
    scene.isPaused = false
    sceneView.scene = scene
    sceneView.autoenablesDefaultLighting = true
  }
  
  func initARSession() {
    let config = ARWorldTrackingConfiguration()
    config.worldAlignment = .gravityAndHeading
    config.providesAudioData = false
    config.environmentTexturing = .automatic
    config.planeDetection = .horizontal
    sceneView.session.run(config)
  }
  
  func initCoachingOverlayView() {
    let coachingOverlay = ARCoachingOverlayView()
    coachingOverlay.session = self.sceneView.session
    coachingOverlay.delegate = self
    coachingOverlay.activatesAutomatically = true
    coachingOverlay.goal = .horizontalPlane
    self.sceneView.addSubview(coachingOverlay)
    
    coachingOverlay.translatesAutoresizingMaskIntoConstraints = false
    NSLayoutConstraint.activate([
      NSLayoutConstraint(
        item:  coachingOverlay, attribute: .top, relatedBy: .equal,
        toItem: self.view, attribute: .top, multiplier: 1, constant: 0),
      NSLayoutConstraint(
        item:  coachingOverlay, attribute: .bottom, relatedBy: .equal,
        toItem: self.view, attribute: .bottom, multiplier: 1, constant: 0),
      NSLayoutConstraint(
        item:  coachingOverlay, attribute: .leading, relatedBy: .equal,
        toItem: self.view, attribute: .leading, multiplier: 1, constant: 0),
      NSLayoutConstraint(
        item:  coachingOverlay, attribute: .trailing, relatedBy: .equal,
        toItem: self.view, attribute: .trailing, multiplier: 1, constant: 0)
    ])
  }
  
  // MARK: - Load Models
  
  func loadModels() {
    
    let focusScene = SCNScene(named: "art.scnassets/Scenes/FocusScene.scn")!
    focusNode = focusScene.rootNode.childNode( withName: "focus", recursively: false)!
    sceneView.scene.rootNode.addChildNode(focusNode)
  }
  
  // MARK: - Helper Functions
  
  func updateFocusNode() {
    let results = self.sceneView.hitTest(self.focusPoint, types: [.existingPlaneUsingExtent])
    
    if results.count == 1 {
      if let match = results.first {
        let t = match.worldTransform
        self.focusNode.position = SCNVector3( x: t.columns.3.x, y: t.columns.3.y, z: t.columns.3.z)
        focusNode.isHidden = false

        
        if firstTime2 {
            let generator = UIImpactFeedbackGenerator(style: .heavy)
            generator.impactOccurred()
            
            print(sceneView.scene.rootNode.childNodes.count)
            if firstTime {
                okButton.isHidden = false
            }

            
            firstTime2 = false
        }
        
      }
    } else {
      focusNode.isHidden = true
    }
  }
  
  func updateARPlaneNode(
    planeNode: SCNNode, planeAchor: ARPlaneAnchor) {

    // Update Geometry
    let planeGeometry = planeNode.geometry as! SCNPlane
    planeGeometry.width = CGFloat(planeAchor.extent.x)
    planeGeometry.height = CGFloat(planeAchor.extent.z)
    
    // Update Position
    planeNode.position = SCNVector3Make(planeAchor.center.x, 0, planeAchor.center.z)
  }
  
  func createARPlaneNode(planeAnchor: ARPlaneAnchor) -> SCNNode {
    
    // Create Geometry
    let planeGeometry = SCNPlane(width: CGFloat(planeAnchor.extent.x), height: CGFloat(planeAnchor.extent.z))
    
    // Create plane node
    let planeNode = SCNNode(geometry: planeGeometry)
    planeNode.position = SCNVector3Make(planeAnchor.center.x, 0, planeAnchor.center.z)
    planeNode.transform = SCNMatrix4MakeRotation(-Float.pi / 2, 1, 0, 0)
    planeNode.isHidden = true
   
    return planeNode
  }
  
}


extension ARViewController : ARSCNViewDelegate, ARCoachingOverlayViewDelegate {
  
  // MARK: - SceneKit Management
    func coachingOverlayViewWillActivate(_ coachingOverlayView: ARCoachingOverlayView) {
        firstTime2 = true
        focusNode.isHidden = true
    }
  
  func renderer(_ renderer: SCNSceneRenderer,
                updateAtTime time: TimeInterval) {
    DispatchQueue.main.async {
      self.updateFocusNode()
      if self.nodeActive {
        self.selectedNode.position = self.focusNode.position
        
      }
    }
  }
  
  // MARK: - Session State Management
  
  func session(_ session: ARSession, cameraDidChangeTrackingState camera: ARCamera) {
  }
  
  // MARK: - Session Error Managent
  
  func session(_ session: ARSession, didFailWithError error: Error) {
  }
  
  func sessionWasInterrupted(_ session: ARSession) {
  }
  
  func sessionInterruptionEnded(_ session: ARSession) {
  }
  
  // MARK: - Plane Management
  
  func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
    guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
    DispatchQueue.main.async {
        let planeNode = self.createARPlaneNode(planeAnchor: planeAnchor)
        node.addChildNode(planeNode)
    }
  }
  
  func renderer(_ renderer: SCNSceneRenderer, didUpdate node: SCNNode, for anchor: ARAnchor) {
    guard let planeAnchor = anchor as? ARPlaneAnchor else { return }
    DispatchQueue.main.async {
        self.updateARPlaneNode(planeNode: node.childNodes[0], planeAchor: planeAnchor)
    }
  }
    
}


