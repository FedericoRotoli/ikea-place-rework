//
//  ProfileController.swift
//  Ikea Place
//
//  Created by Federico Rotoli on 06/12/2019.
//  Copyright © 2019 Federico Rotoli. All rights reserved.
//

import Foundation
import UIKit

class ProfileController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    @IBAction func backButton(_ sender: Any) {
        dismiss(animated: true, completion: {})
    }
}
